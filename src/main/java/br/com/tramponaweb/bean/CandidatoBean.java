package br.com.tramponaweb.bean;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.Serializable;
import java.util.Date;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;

import br.com.tramponaweb.lazy.CandidatoLazy;
import br.com.tramponaweb.model.Candidato;
import br.com.tramponaweb.model.Escolaridade;
import br.com.tramponaweb.model.FormaContratacao;
import br.com.tramponaweb.service.CandidatoService;

@Named
@ViewScoped
public class CandidatoBean implements Serializable {

	private static final long serialVersionUID = 1L;
	private Candidato candidato;
	@Inject
	private CandidatoLazy candidatoLazy;
	@Inject
	private CandidatoService candidatoService;

	private UploadedFile file;
	private StreamedContent imgVisualizar;

	public CandidatoBean() {
		this.candidato = new Candidato();
	}

	public void limpar() {
		this.candidato = new Candidato();
	}

	public void salvar() {
		try {
			candidatoService.salvar(candidato);
			limpar();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void excluir(Long id) {
		try {
			candidatoService.excluir(id);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void upload(FileUploadEvent event) {
		file = event.getFile();
		if (file != null) {
			try {
				// Transformar a imagem em bytes para salvar em banco de dados
				byte[] bimagem = event.getFile().getContents();
				this.candidato.setFoto(bimagem);
				FacesContext instance = FacesContext.getCurrentInstance();
				instance.addMessage("mensagens", new FacesMessage(
						FacesMessage.SEVERITY_ERROR, file.getFileName()
								+ " anexado com sucesso", null));
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public void visualizar(Long id) {
		candidato = candidatoService.obterPorId(id);
		imgVisualizar = mostrarImagemDeByte();
	}

	public StreamedContent mostrarImagemDeByte() {
		InputStream in = null;
		StreamedContent sc;
		if (candidato.getFoto() != null) {
			in = new ByteArrayInputStream(candidato.getFoto());
		}

		if (in != null) {
			sc = new DefaultStreamedContent(in, "image/png");
		} else {
			sc = null;
		}
		return sc;
	}

	public StreamedContent getFoto() {
		InputStream in = null;
		StreamedContent sc;
		if (candidato.getFoto() != null) {
			in = new ByteArrayInputStream(candidato.getFoto());
		}

		if (in != null) {
			sc = new DefaultStreamedContent(in);
		} else {
			sc = null;
		}
		return sc;
	}

	public Candidato getCandidato() {
		return candidato;
	}

	public void setCandidato(Candidato candidato) {
		this.candidato = candidato;
	}

	public CandidatoLazy getCandidatoLazy() {
		return candidatoLazy;
	}

	public void setCandidatoLazy(CandidatoLazy candidatoLazy) {
		this.candidatoLazy = candidatoLazy;
	}

	public Escolaridade[] getEscolaridadeCombo() {
		return Escolaridade.values();
	}

	public FormaContratacao[] getFormaContratacaoCombo() {
		return FormaContratacao.values();
	}

	public Date getMaxDataNascimento() {
		return new Date();
	}

	public UploadedFile getFile() {
		return file;
	}

	public void setFile(UploadedFile file) {
		this.file = file;
	}

	public StreamedContent getImgVisualizar() {
		return imgVisualizar;
	}

	public void setImgVisualizar(StreamedContent imgVisualizar) {
		this.imgVisualizar = imgVisualizar;
	}

}
