package br.com.tramponaweb.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import br.com.tramponaweb.dao.QuestionarioDAO;
import br.com.tramponaweb.lazy.QuestionarioLazy;
import br.com.tramponaweb.model.Pergunta;
import br.com.tramponaweb.model.Questionario;
import br.com.tramponaweb.model.TipoPergunta;

@SuppressWarnings("serial")
@Named
@ViewScoped
public class QuestionarioBean implements Serializable {

    private Questionario questionarioTela;
    @Inject
    private QuestionarioLazy questionarioLazy;
    private Pergunta perguntaTela;
    private List<Pergunta> perguntas;
    private int indiceEdicaoPergunta = -1;
    @Inject
    private QuestionarioDAO questionarioDAO;

    public QuestionarioBean() {

    }

    @PostConstruct
    public void init() {
	questionarioTela = new Questionario();
	perguntas = new ArrayList<Pergunta>();
	perguntaTela = new Pergunta();
    }

    public void salvar() {
	questionarioDAO.salvar(questionarioTela);
    }

    public void novaPergunta() {
	perguntaTela = new Pergunta();
	indiceEdicaoPergunta = -1;
    }

    public void novoQuestionario() {
	questionarioTela = new Questionario();
    }

    public void exibirMsgValidacao(String msg) {
	FacesContext.getCurrentInstance().validationFailed();
	FacesContext.getCurrentInstance().addMessage("msg",
		new FacesMessage(FacesMessage.SEVERITY_ERROR, msg, msg));
    }

    public void adicionarPergunta() {
	if (indiceEdicaoPergunta != -1) {
	    Pergunta antiga = getPerguntas().remove(indiceEdicaoPergunta);
	    if (getPerguntas().contains(perguntaTela)) {
		exibirMsgValidacao("Pergunta ja cadastrada.");
		getPerguntas().add(indiceEdicaoPergunta, antiga);
		return;
	    }
	    getPerguntas().add(indiceEdicaoPergunta, perguntaTela);
	} else {
	    if (getPerguntas().contains(perguntaTela)) {
		exibirMsgValidacao("Pergunta ja cadastrada.");
		return;
	    }
	    getPerguntas().add(perguntaTela);
	}
    }

    public void removerPergunta(Pergunta perguntaRemover) {
	perguntas.remove(perguntaRemover);
    }

    public void removerQuestionario(Long id) {
	questionarioDAO.excluir(id);
    }

    public void editarPergunta(Pergunta pergunta) {
	indiceEdicaoPergunta = perguntas.indexOf(pergunta);
	if(indiceEdicaoPergunta!=-1){
	    Pergunta copia = new Pergunta(pergunta.getId(), pergunta.getDescricao(), pergunta.getTipoPergunta());
	    perguntaTela = copia;
	}

    }

    public void editarQuestionario(Long id) {
	questionarioTela = questionarioDAO.obterPorId(id);
	setPerguntas(questionarioTela.getPerguntas());
    }

    public TipoPergunta[] getTipoPerguntaCombo() {
	return TipoPergunta.values();
    }

    public Questionario getQuestionarioTela() {
	return questionarioTela;
    }

    public void setQuestionarioTela(Questionario questionarioTela) {
	this.questionarioTela = questionarioTela;
    }

    public Pergunta getPerguntaTela() {
	return perguntaTela;
    }

    public void setPerguntaTela(Pergunta perguntaTela) {
	this.perguntaTela = perguntaTela;
    }

    public List<Pergunta> getPerguntas() {
	return perguntas;
    }

    public void setPerguntas(List<Pergunta> perguntas) {
	this.perguntas = perguntas;
    }

    /**
     * @return the questionarioLazy
     */
    public QuestionarioLazy getQuestionarioLazy() {
        return questionarioLazy;
    }

    /**
     * @param questionarioLazy the questionarioLazy to set
     */
    public void setQuestionarioLazy(QuestionarioLazy questionarioLazy) {
        this.questionarioLazy = questionarioLazy;
    }
    

}
