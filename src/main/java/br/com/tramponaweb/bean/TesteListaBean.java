package br.com.tramponaweb.bean;

import java.io.Serializable;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import br.com.tramponaweb.model.Candidato;
import br.com.tramponaweb.model.Escolaridade;

@ManagedBean
@ViewScoped
public class TesteListaBean implements Serializable {
    private static final long serialVersionUID = 1L;
    List<Candidato> lista;
    boolean exibeTabela;
    Escolaridade escolaridadeSelecionada;

    public void alteraExibicaoTabela() {
	exibeTabela = escolaridadeSelecionada!=null && escolaridadeSelecionada.name().equals(Escolaridade.FUNDAMENTAL.name());
    }

    public Escolaridade[] getEscolaridadeCombo() {
	return Escolaridade.values();
    }

    public List<Candidato> getLista() {
	return lista;
    }

    public void setLista(List<Candidato> lista) {
	this.lista = lista;
    }

    public boolean isExibeTabela() {
	return exibeTabela;
    }

    public void setExibeTabela(boolean exibeTabela) {
	this.exibeTabela = exibeTabela;
    }

    public Escolaridade getEscolaridadeSelecionada() {
	return escolaridadeSelecionada;
    }

    public void setEscolaridadeSelecionada(Escolaridade escolaridadeSelecionada) {
	this.escolaridadeSelecionada = escolaridadeSelecionada;
    }

}
