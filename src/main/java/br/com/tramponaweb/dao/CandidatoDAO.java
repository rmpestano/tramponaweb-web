package br.com.tramponaweb.dao;

import java.io.Serializable;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceUnit;

import br.com.tramponaweb.model.Candidato;

@Stateless
public class CandidatoDAO implements Serializable {
    private static final long serialVersionUID = 1L;

//    @PersistenceUnit(name = "tramponaweb")
    private EntityManager entityManager;

    public CandidatoDAO() {
    }

    public CandidatoDAO(EntityManager entityManager) {
	this.entityManager = entityManager;
    }

    @SuppressWarnings("unchecked")
    public List<Candidato> listarTodos() {
	return entityManager.createQuery("select c FROM Candidato c")
		.getResultList();
    }

    public EntityManager getEntityManager() {
	return entityManager;
    }

    public void setEntityManager(EntityManager entityManager) {
	this.entityManager = entityManager;
    }

    public void salvar(Candidato candidato) {
	try {
	    entityManager.merge(candidato);
	} catch (Exception e) {
	    e.printStackTrace();
	}
    }

    public void excluir(Long id) {
	try {
	    Candidato candidatoRemove = obterPorId(id);
	    entityManager.remove(candidatoRemove);
	} catch (Exception e) {
	    e.printStackTrace();
	}
    }

    public Candidato obterPorId(Long id) {
	return entityManager.find(Candidato.class, id);
    }
}
