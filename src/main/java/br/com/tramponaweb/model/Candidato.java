package br.com.tramponaweb.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="candidato")
public class Candidato implements Serializable {

    /**
	 * 
	 */
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private Long id;
    @Column(name="nome")
    private String nome;
    @Column(name="endereco")
    private String end;
    @Column(name="telefone")
    private String tel;
    @Temporal(TemporalType.DATE)
    @Column(name="data_nascimento")
    private Date dataNasc;
    @Column(name="escolaridade")
    @Enumerated(EnumType.ORDINAL)
    private Escolaridade escolaridade;
    @Column(name="forma_contratacao")
    @Enumerated(EnumType.ORDINAL)
    private FormaContratacao formaContratacao;
    @Column(name="email")
    private String email;
    @Column(name="foto")
    @Lob
    private byte[] foto;
    

    public Candidato() {
    }

    public String getNome() {
	return nome;
    }

    public void setNome(String nome) {
	this.nome = nome;
    }

    public String getEnd() {
	return end;
    }

    public void setEnd(String end) {
	this.end = end;
    }

    public String getTel() {
	return tel;
    }

    public void setTel(String tel) {
	this.tel = tel;
    }

    public Date getDataNasc() {
	return dataNasc;
    }

    public void setDataNasc(Date dataNasc) {
	this.dataNasc = dataNasc;
    }

    public Long getId() {
	return id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((id == null) ? 0 : id.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (!(obj instanceof Candidato))
	    return false;
	Candidato other = (Candidato) obj;
	if (id == null) {
	    if (other.id != null)
		return false;
	} else if (!id.equals(other.id))
	    return false;
	return true;
    }

    public Escolaridade getEscolaridade() {
	return escolaridade;
    }

    public void setEscolaridade(Escolaridade escolaridade) {
	this.escolaridade = escolaridade;
    }

    public FormaContratacao getFormaContratacao() {
	return formaContratacao;
    }

    public void setFormaContratacao(FormaContratacao formaContratacao) {
	this.formaContratacao = formaContratacao;
    }

    public String getEmail() {
	return email;
    }

    public void setEmail(String email) {
	this.email = email;
    }

    public byte[] getFoto() {
	return foto;
    }

    public void setFoto(byte[] foto) {
	this.foto = foto;
    }

    @Override
    public String toString() {
	StringBuilder builder = new StringBuilder();
	builder.append("Candidato [id=").append(id).append(", nome=")
		.append(nome).append(", end=").append(end).append(", tel=")
		.append(tel).append(", dataNasc=").append(dataNasc)
		.append(", escolaridade=").append(escolaridade)
		.append(", formaContratacao=").append(formaContratacao)
		.append(", email=").append(email).append("]");
	return builder.toString();
    }

}
