package br.com.tramponaweb.model;

public enum Escolaridade {
    FUNDAMENTAL("Fundamental"), MEDIO("Médio"), SUPERIOR("Superior");
    private String label;

    private Escolaridade(String label) {
	this.label = label;
    }

    public String getLabel() {
	return label;
    }
}
