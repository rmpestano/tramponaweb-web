package br.com.tramponaweb.producer;

import org.apache.deltaspike.core.api.exclude.Exclude;
import org.apache.deltaspike.core.api.projectstage.ProjectStage;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Disposes;
import javax.enterprise.inject.Produces;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;

@ApplicationScoped
@Exclude(ifProjectStage = ProjectStage.UnitTest.class)
public class EntityManagerProducer {
    @PersistenceUnit(name = "tramponaweb", unitName = "tramponaweb")
    private EntityManagerFactory emf;

    @Produces
    @RequestScoped
    // you can also make this @RequestScoped
    public EntityManager create() {
	return emf.createEntityManager();
    }

    public void close(@Disposes EntityManager em) {
	if (em.isOpen()) {
	    em.close();
	}
    }
}