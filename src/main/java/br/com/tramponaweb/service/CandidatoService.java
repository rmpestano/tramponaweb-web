package br.com.tramponaweb.service;

import java.util.List;

import br.com.tramponaweb.model.Candidato;

public interface CandidatoService {

    public void salvar(Candidato candidato);

    public Candidato obterPorId(Long id);

    public List<Candidato> listar();

    public void excluir(Long id);
}
